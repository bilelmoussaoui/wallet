mod tickets;
mod window;

pub use tickets::TicketRow;
pub use window::{View, Window};
