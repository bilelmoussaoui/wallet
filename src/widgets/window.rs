use crate::application::Action;
use crate::config;
use crate::models::{NewTicket, ObjectWrapper, Ticket, TicketsModel};
use crate::widgets::TicketRow;
use gio::prelude::*;
use glib::Sender;
use gtk::prelude::*;
use std::rc::Rc;

pub enum View {
    Empty,
    Tickets,
    Ticket,
}

pub struct Window {
    pub widget: gtk::ApplicationWindow,
    builder: gtk::Builder,
    sender: Sender<Action>,
    tickets_model: TicketsModel,
}

impl Window {
    pub fn new(sender: Sender<Action>) -> Rc<Self> {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/window.ui");
        let widget: gtk::ApplicationWindow = builder.get_object("window").expect("Failed to find the window object");
        let tickets_model = TicketsModel::new();

        let window = Rc::new(Self {
            widget,
            builder,
            sender,
            tickets_model,
        });

        if config::PROFILE == "Devel" {
            window.widget.get_style_context().add_class("devel");
        }

        window.setup_actions(window.clone());
        window.setup_widgets(window.clone());
        window
    }

    pub fn close_notification(&self) {
        let notification_msg: gtk::Label = self.builder.get_object("notification_msg").expect("Failed to get notification_msg");
        let notification: gtk::Revealer = self.builder.get_object("notification").expect("Failed to get notification");
        notification_msg.set_text(&"");
        notification.set_reveal_child(false);
    }

    pub fn notify(&self, message: String) {
        let notification_msg: gtk::Label = self.builder.get_object("notification_msg").expect("Failed to get notification_msg");
        let notification: gtk::Revealer = self.builder.get_object("notification").expect("Failed to get notification");
        notification_msg.set_text(&message);
        notification.set_reveal_child(true);
    }

    pub fn add_ticket(&self, ticket: Ticket) {
        self.tickets_model.add(&ticket);
    }

    pub fn delete_ticket(&self, ticket: Ticket) {
        self.tickets_model.delete(&ticket);
    }

    pub fn load_ticket(&self, new_ticket: NewTicket) {
        self.set_view(View::Ticket);
        let data_label: gtk::Label = self.builder.get_object("data_label").expect("Failed to get data_label");
        let name_entry: gtk::Entry = self.builder.get_object("name_entry").expect("Failed to get name_entry");
        let description_entry: gtk::Entry = self.builder.get_object("description_entry").expect("Failed to get description_entry");

        data_label.set_text(&new_ticket.data);
    }

    pub fn set_view(&self, view: View) {
        let main_stack: gtk::Stack = self.builder.get_object("main_stack").expect("Failed to get main_stack");
        let headerbar_stack: gtk::Stack = self.builder.get_object("headerbar_stack").expect("Failed to get headerbar_stack");

        match view {
            View::Empty => {
                main_stack.set_visible_child_name("empty_state");
                headerbar_stack.set_visible_child_name("normal_state");
            }
            View::Tickets => {
                main_stack.set_visible_child_name("tickets_state");
                headerbar_stack.set_visible_child_name("normal_state");
            }
            View::Ticket => {
                main_stack.set_visible_child_name("ticket_view");
                headerbar_stack.set_visible_child_name("ticket_view");
            }
        }
    }

    pub fn refresh_view(&self) {
        if self.tickets_model.get_count() != 0 {
            self.set_view(View::Tickets);
        } else {
            self.set_view(View::Empty);
        }
    }

    fn setup_actions(&self, w: Rc<Self>) {
        let actions = gio::SimpleActionGroup::new();
        let scan_qr = gio::SimpleAction::new("scan-qr", None);
        let sender = self.sender.clone();
        scan_qr.connect_activate(move |_, _| {
            sender.send(Action::ScanQR).expect("Failed to Scan the QR code");
        });
        actions.add_action(&scan_qr);

        let back = gio::SimpleAction::new("back", None);
        let sender = self.sender.clone();
        let window = w.clone();
        back.connect_activate(move |_, _| {
            window.refresh_view();
        });
        actions.add_action(&back);

        let close_notification = gio::SimpleAction::new("close-notification", None);
        let sender = self.sender.clone();
        close_notification.connect_activate(move |_, _| {
            sender.send(Action::CloseNotification).expect("Failed to close the notification");
        });
        actions.add_action(&close_notification);

        self.widget.insert_action_group("window", Some(&actions));
    }

    fn setup_widgets(&self, w: Rc<Self>) {
        let listbox: gtk::ListBox = self.builder.get_object("tickets_listbox").expect("Failed to get tickets_listbox");
        let sender = self.sender.clone();
        listbox.bind_model(Some(&self.tickets_model.model), move |ticket| {
            let ticket: Ticket = ticket.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
            println!("{:#?}", ticket.data);
            let row = TicketRow::new(ticket, sender.clone());
            row.widget.upcast::<gtk::Widget>()
        });

        listbox.set_header_func(Some(Box::new(move |row1, row2| {
            if let Some(_) = row2 {
                let separator = gtk::Separator::new(gtk::Orientation::Horizontal);
                row1.set_header(Some(&separator));
                separator.show();
            }
        })));
        let window = w.clone();
        self.tickets_model.model.connect_items_changed(move |_, _, _, _| {
            window.refresh_view();
        });
        self.refresh_view();
    }
}
