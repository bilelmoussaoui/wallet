use crate::models::{MailToTicket, Ticket, TicketViewable};
use failure::Error;
use gtk::prelude::*;
use url::Url;

impl MailToTicket {
    pub fn new(ticket: &Ticket) -> Result<Self, Error> {
        let uri = Url::parse(&ticket.data)?;
        if uri.scheme() == "mailto" {
            let tel_data: Vec<&str> = ticket.data.split(":").collect();

            let mut email = String::from("");
            email.push_str(tel_data.get(1).unwrap());

            return Ok(Self { email });
        }
        bail!("Failed to parse MailToTicket")
    }
}

impl TicketViewable for MailToTicket {
    fn get_widget(&self) -> gtk::Box {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/mailto_ticket.ui");
        let container: gtk::Box = builder.get_object("mailto_ticket").expect("Failed to get mailto_ticket");

        let email_label: gtk::Label = builder.get_object("email_label").expect("Failed to get email_label");
        email_label.set_text(&self.email);

        container
    }
}
