use failure::Error;
use gtk::prelude::*;
use url::Url;

use crate::models::{OAuthTicket, Ticket, TicketViewable};

impl OAuthTicket {
    pub fn new(ticket: &Ticket) -> Result<Self, Error> {
        let uri = Url::parse(&ticket.data)?;
        if uri.scheme() == "otpauth" {
            let mut issuer = String::from("");
            let mut secret = String::from("");
            let mut username = String::from("");

            let pairs = uri.query_pairs();
            for (key, val) in pairs.take(pairs.count()) {
                if key == "issuer" {
                    issuer.push_str(&val);
                } else if key == "secret" {
                    secret.push_str(&val);
                }
            }

            if let Some(mut segments) = uri.path_segments() {
                let issuer_username: Vec<&str> = segments.next().unwrap().split(":").collect();
                username.push_str(&issuer_username.get(1).or(Some(&"")).unwrap().to_string());
                if issuer.is_empty() {
                    issuer.push_str(&issuer_username.get(0).or(Some(&"")).unwrap().to_string());
                }
            }
            return Ok(Self {
                issuer,
                username,
                secret,
                uri: ticket.data.clone(),
            });
        }
        bail!("Failed to parse OAuthTicket")
    }
}

impl TicketViewable for OAuthTicket {
    fn get_widget(&self) -> gtk::Box {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/oauth_ticket.ui");
        let container: gtk::Box = builder.get_object("oauth_ticket").expect("Failed to get oauth_ticket");

        let issuer_label: gtk::Label = builder.get_object("issuer_label").expect("Failed to get issuer_label");
        let username_label: gtk::Label = builder.get_object("username_label").expect("Failed to get username_label");
        let secret_label: gtk::Label = builder.get_object("secret_label").expect("Failed to get secret_label");

        issuer_label.set_text(&self.issuer);
        username_label.set_text(&self.username);
        secret_label.set_text(&self.secret);

        container
    }
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_oauth_ticket() {
        let uri = "otpauth://totp/GitHub:bilelmoussaoui?secret=test&issuer=GitHub".to_string();

        let qrcode = Ticket { data: uri.clone() };
        let ticket = OAuthTicket::new(&qrcode).unwrap();
        // assert_eq!(ticket.issuer, "Github".to_string());
        assert_eq!(ticket.username, "bilelmoussaoui".to_string());
        assert_eq!(ticket.secret, "test".to_string());
        assert_eq!(ticket.uri, uri);
    }
    #[test]
    fn test_failure_oauth_ticket() {
        let file_ticket = OAuthTicket::new(&Ticket {
            data: "file://home/test.png".to_string(),
        });
        assert_eq!(file_ticket.is_err(), true);
        let err_ticket = OAuthTicket::new(&QRCode {
            data: "otpauth://totp/bilelmoussaoui?issuer=GitHub".to_string(),
        });
        assert_eq!(file_ticket.is_err(), true);
    }
}
