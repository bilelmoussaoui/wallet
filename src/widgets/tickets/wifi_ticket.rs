use crate::models::{parser, Ticket, TicketViewable, WifiTicket};
use failure::Error;
use gtk::prelude::*;

impl WifiTicket {
    pub fn new(ticket: &Ticket) -> Result<Self, Error> {
        let content = parser::parse_content(&ticket.data, "wifi:T:${security};S:${network};P:${password};;")?;
        if content.contains_key("security") && content.contains_key("network") && content.contains_key("password") {
            return Ok(Self {
                security: content.get("security").unwrap().to_string(),
                network: content.get("network").unwrap().to_string(),
                password: content.get("password").unwrap().to_string(),
            });
        }
        bail!("Failed to parse WifiTicket")
    }
}

impl TicketViewable for WifiTicket {
    fn get_widget(&self) -> gtk::Box {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/wifi_ticket.ui");
        let container: gtk::Box = builder.get_object("wifi_ticket").expect("Failed to get wifi_ticket");

        let security_label: gtk::Label = builder.get_object("security_label").expect("Failed to get security_label");
        security_label.set_text(&self.security);

        let network_label: gtk::Label = builder.get_object("network_label").expect("Failed to get network_label");
        network_label.set_text(&self.network);

        let password_label: gtk::Label = builder.get_object("password_label").expect("Failed to get password_label");
        password_label.set_text(&self.password);
        container
    }
}
