use crate::models::{Ticket, TicketViewable, URLTicket};
use failure::Error;
use gtk::prelude::*;
use url::Url;

impl TicketViewable for URLTicket {
    fn get_widget(&self) -> gtk::Box {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/url_ticket.ui");
        let container: gtk::Box = builder.get_object("url_ticket").expect("Failed to get url_ticket");

        let url_label: gtk::Label = builder.get_object("url_label").expect("Failed to get url_label");
        url_label.set_text(&self.url.as_str());

        container
    }
}

/* URLTicket */

impl URLTicket {
    pub fn new(ticket: &Ticket) -> Result<Self, Error> {
        Ok(URLTicket { url: Url::parse(&ticket.data)? })
    }
}
