use crate::application::Action;
use crate::models::Ticket;
use gio::prelude::*;
use glib::Sender;
use gtk::prelude::*;

pub struct TicketRow {
    pub widget: gtk::ListBoxRow,
    builder: gtk::Builder,
    sender: Sender<Action>,
    ticket: Ticket,
}

impl TicketRow {
    pub fn new(ticket: Ticket, sender: Sender<Action>) -> Self {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/ticket_row.ui");
        let widget: gtk::ListBoxRow = builder.get_object("ticket_row").expect("Failed to get ticket_row");

        let ticket_row = Self { widget, builder, sender, ticket };
        ticket_row.init();
        ticket_row.setup_actions();
        ticket_row
    }

    fn init(&self) {
        let event_box: gtk::EventBox = self.builder.get_object("eventbox").expect("Failed to load eventbox");
        let qrcode_revealer: gtk::Revealer = self.builder.get_object("qrcode_revealer").expect("Failed to load qrcode_revealer");
        event_box.connect_button_press_event(move |_, _| {
            qrcode_revealer.set_reveal_child(!qrcode_revealer.get_reveal_child());
            gtk::Inhibit(false)
        });

        let ticket_widget: gtk::Box = self.builder.get_object("ticket_widget").expect("Failed to load ticket_widget");
        if let Ok(viewable) = self.ticket.get_viewable() {
            let widget = viewable.get_widget();
            widget.get_style_context().add_class("ticket-row");
            ticket_widget.add(&widget);
        }

        let title_label: gtk::Label = self.builder.get_object("title_label").expect("Failed to load title_label");
        let description_label: gtk::Label = self.builder.get_object("description_label").expect("Failed to load description_label");
        let created_at_label: gtk::Label = self.builder.get_object("created_at_label").expect("Failed to load created_at_label");
        title_label.set_text(&self.ticket.title);
        description_label.set_text(&self.ticket.description);
        created_at_label.set_text(&self.ticket.created_at);
    }

    fn setup_actions(&self) {
        let actions = gio::SimpleActionGroup::new();
        let delete = gio::SimpleAction::new("delete", None);
        let sender = self.sender.clone();
        let ticket = self.ticket.clone();
        delete.connect_activate(move |_, _| {
            sender.send(Action::DeleteTicket(ticket.clone())).expect("Failed to delete ticket");
        });
        actions.add_action(&delete);

        let edit = gio::SimpleAction::new("edit", None);
        let sender = self.sender.clone();
        let ticket = self.ticket.clone();
        edit.connect_activate(move |_, _| {
            sender.send(Action::EditTicket(ticket.clone())).expect("Failed to edit ticket");
        });
        actions.add_action(&edit);

        self.widget.insert_action_group("ticket", Some(&actions));
    }
}
