use crate::models::{parser, ContactTicket, Ticket, TicketViewable};
use failure::Error;
use gtk::prelude::*;

impl ContactTicket {
    pub fn new(ticket: &Ticket) -> Result<Self, Error> {
        let content = parser::parse_content(&ticket.data, "MECARD:N:${name};EMAIL:${email};;")?;
        if content.contains_key("name") && content.contains_key("email") {
            return Ok(Self {
                name: content.get("name").unwrap().to_string(),
                email: content.get("email").unwrap().to_string(),
            });
        }
        bail!("Failed to parse ContactTicket")
    }
}

impl TicketViewable for ContactTicket {
    fn get_widget(&self) -> gtk::Box {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/contact_ticket.ui");
        let container: gtk::Box = builder.get_object("contact_ticket").expect("Failed to get contact_ticket");

        let name_label: gtk::Label = builder.get_object("name_label").expect("Failed to get name_label");
        name_label.set_text(&self.name);

        let email_label: gtk::Label = builder.get_object("email_label").expect("Failed to get email_label");
        email_label.set_text(&self.email);

        container
    }
}
