use crate::models::{parser, EventTicket, Ticket, TicketViewable};
use chrono::NaiveDate;
use failure::Error;
use gtk::prelude::*;

impl EventTicket {
    pub fn new(ticket: &Ticket) -> Result<Self, Error> {
        let content = parser::parse_content(
            &ticket.data,
            "BEGIN:VEVENT
            SUMMARY:${name}
            DTSTART:${start_at}
            DTEND:${end_at}
            END:VEVENT",
        )?;

        if content.contains_key("name") && content.contains_key("start_at") && content.contains_key("end_at") {
            return Ok(Self {
                name: content.get("name").unwrap().to_string(),
                start_at: content.get("start_at").unwrap().to_string(),
                end_at: content.get("end_at").unwrap().to_string(),
            });
        }
        bail!("Failed to parse EventTicket")
    }
}

impl TicketViewable for EventTicket {
    fn get_widget(&self) -> gtk::Box {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/event_ticket.ui");
        let container: gtk::Box = builder.get_object("event_ticket").expect("Failed to get event_ticket");

        let name_label: gtk::Label = builder.get_object("name_label").expect("Failed to get name_label");
        name_label.set_text(&self.name);

        let start_at_label: gtk::Label = builder.get_object("start_at_label").expect("Failed to get start_at_label");
        if let Ok(start_at) = parse_date(&self.start_at, "%Y%m%d") {
            start_at_label.set_text(&start_at);
        }

        let end_at_label: gtk::Label = builder.get_object("end_at_label").expect("Failed to get end_at_label");
        if let Ok(end_at) = parse_date(&self.end_at, "%Y%m%d") {
            end_at_label.set_text(&end_at);
        }
        container
    }
}

fn parse_date(date: &str, format: &str) -> Result<String, Error> {
    let time = NaiveDate::parse_from_str(date, format)?;
    Ok(time.format("%Y-%m-%d").to_string())
}
