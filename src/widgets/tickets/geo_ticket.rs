use crate::models::{parser, GeoTicket, Ticket, TicketViewable};
use failure::Error;
use gtk::prelude::*;

impl GeoTicket {
    pub fn new(ticket: &Ticket) -> Result<Self, Error> {
        let content = parser::parse_content(&ticket.data, "geo:${latitude},${longitude}")?;
        if content.contains_key("latitude") && content.contains_key("longitude") {
            return Ok(Self {
                latitude: content.get("latitude").unwrap().to_string(),
                longitude: content.get("longitude").unwrap().to_string(),
            });
        }
        bail!("Failed to parse GeoTicket")
    }
}

impl TicketViewable for GeoTicket {
    fn get_widget(&self) -> gtk::Box {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/geo_ticket.ui");
        let container: gtk::Box = builder.get_object("geo_ticket").expect("Failed to get geo_ticket");

        let latitude_label: gtk::Label = builder.get_object("latitude_label").expect("Failed to get latitude_label");
        latitude_label.set_text(&self.latitude);

        let longitude_label: gtk::Label = builder.get_object("longitude_label").expect("Failed to get longitude_label");
        longitude_label.set_text(&self.longitude);

        container
    }
}
