use gtk::prelude::*;

use crate::models::{DefaultTicket, Ticket, TicketViewable};

impl DefaultTicket {
    pub fn new(ticket: &Ticket) -> Self {
        Self { data: ticket.data.clone() }
    }
}

impl TicketViewable for DefaultTicket {
    fn get_widget(&self) -> gtk::Box {
        let container = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        container.set_border_width(12);

        let data_label = gtk::Label::new(Some(&self.data));
        data_label.set_halign(gtk::Align::Start);
        data_label.set_valign(gtk::Align::Start);
        data_label.show();

        container.pack_start(&data_label, false, false, 0);
        container.show();
        container
    }
}
