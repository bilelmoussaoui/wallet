use crate::models::{SmsToTicket, Ticket, TicketViewable};
use failure::Error;
use gtk::prelude::*;
use url::Url;

impl SmsToTicket {
    pub fn new(ticket: &Ticket) -> Result<Self, Error> {
        let uri = Url::parse(&ticket.data)?;
        if uri.scheme() == "smsto" {
            let tel_data: Vec<&str> = ticket.data.split(":").collect();

            let mut phone = String::from("");
            phone.push_str(tel_data.get(1).unwrap());

            let mut content = String::from("");
            content.push_str(tel_data.get(2).unwrap());

            return Ok(Self { phone, content });
        }
        bail!("Failed to parse SmsToTicket")
    }
}

impl TicketViewable for SmsToTicket {
    fn get_widget(&self) -> gtk::Box {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/smsto_ticket.ui");
        let container: gtk::Box = builder.get_object("smsto_ticket").expect("Failed to get smsto_ticket");

        let phone_label: gtk::Label = builder.get_object("phone_label").expect("Failed to get phone_label");
        phone_label.set_text(&self.phone);

        let content_label: gtk::Label = builder.get_object("content_label").expect("Failed to get content_label");
        content_label.set_text(&self.content);

        container
    }
}
