use crate::models::{TelTicket, Ticket, TicketViewable};
use failure::Error;
use gtk::prelude::*;
use url::Url;

impl TelTicket {
    pub fn new(ticket: &Ticket) -> Result<Self, Error> {
        let uri = Url::parse(&ticket.data)?;
        if uri.scheme() == "tel" {
            let tel_data: Vec<&str> = ticket.data.split(":").collect();

            let mut telephone = String::from("");
            telephone.push_str(tel_data.get(1).unwrap());

            return Ok(Self { telephone });
        }
        bail!("Failed to parse TelTicket")
    }
}

impl TicketViewable for TelTicket {
    fn get_widget(&self) -> gtk::Box {
        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/tel_ticket.ui");
        let container: gtk::Box = builder.get_object("tel_ticket").expect("Failed to get tel_ticket");

        let tel_label: gtk::Label = builder.get_object("tel_label").expect("Failed to get tel_label");
        tel_label.set_text(&self.telephone);

        container
    }
}
