extern crate image;
extern crate zbar_rust;
use failure::Error;

use image::GenericImageView;
use zbar_rust::ZBarImageScanner;

pub fn scan_image(image_path: &str) -> Result<String, Error> {
    let img = image::open(image_path)?;
    let (width, height) = img.dimensions();
    println!("{}, {}", width, height);
    let luma_img = img.to_luma();
    let luma_img_data: Vec<u8> = luma_img.to_vec();

    let mut scanner = ZBarImageScanner::new();
    let results = scanner.scan_y800(&luma_img_data, width, height).unwrap();
    if let Some(first_result) = &results.get(0) {
        return Ok(String::from_utf8(first_result.data.to_vec())?);
    }
    bail!("Failed to detect a QRCode")
}
