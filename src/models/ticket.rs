use crate::database;
use crate::schema::tickets;
use diesel::prelude::*;
use diesel::RunQueryDsl;
use url::Url;

use failure::Error;

#[derive(Queryable, PartialEq, Debug, Clone, Serialize, Deserialize)]
pub struct Ticket {
    pub id: i32,
    pub title: String,
    pub description: String,
    pub data: String,
    pub created_at: String,
}

#[derive(Insertable)]
#[table_name = "tickets"]
pub struct NewTicket {
    pub title: String,
    pub description: String,
    pub data: String,
}

impl database::Insert<Ticket> for NewTicket {
    type Error = database::Error;
    fn insert(&self) -> Result<Ticket, database::Error> {
        let db = database::connection();
        let conn = db.get()?;

        diesel::insert_into(tickets::table).values(self).execute(&conn)?;

        tickets::table.order(tickets::columns::id.desc()).first::<Ticket>(&conn).map_err(From::from)
    }
}

impl database::Delete<Ticket> for Ticket {
    type Error = database::Error;
    fn delete(&self) -> Result<(), database::Error> {
        let db = database::connection();
        let conn = db.get()?;
        diesel::delete(tickets::table.filter(tickets::columns::id.eq(self.id))).execute(&conn)?;
        Ok(())
    }
}

impl Ticket {
    pub fn get_viewable(&self) -> Result<Box<TicketViewable>, Error> {
        if let Ok(uri) = Url::parse(&self.data) {
            if uri.scheme() == "mailto" {
                return Ok(Box::new(MailToTicket::new(&self)?));
            } else if uri.scheme() == "wifi" {
                return Ok(Box::new(WifiTicket::new(&self)?));
            } else if uri.scheme() == "geo" {
                return Ok(Box::new(GeoTicket::new(&self)?));
            } else if uri.scheme() == "tel" {
                return Ok(Box::new(TelTicket::new(&self)?));
            } else if uri.scheme() == "smsto" {
                return Ok(Box::new(SmsToTicket::new(&self)?));
            } else if uri.scheme() == "mecard" {
                return Ok(Box::new(ContactTicket::new(&self)?));
            } else if uri.scheme() == "oauth" {
                return Ok(Box::new(OAuthTicket::new(&self)?));
            } else if uri.scheme() == "http" || uri.scheme() == "https" {
                return Ok(Box::new(URLTicket::new(&self)?));
            }
        }
        if self.data.starts_with("BEGIN:VEVENT") {
            return Ok(Box::new(EventTicket::new(&self)?));
        }
        Ok(Box::new(DefaultTicket::new(&self)))
    }
}

pub trait TicketViewable {
    fn get_widget(&self) -> gtk::Box;
}

/* DefaultTicket */
pub struct DefaultTicket {
    pub data: String,
}

pub struct URLTicket {
    pub url: Url,
}

pub struct TelTicket {
    pub telephone: String,
}

pub struct OAuthTicket {
    pub issuer: String,
    pub username: String,
    pub secret: String,
    pub uri: String,
}

pub struct GeoTicket {
    pub latitude: String,
    pub longitude: String,
}

pub struct SmsToTicket {
    pub phone: String,
    pub content: String,
}

pub struct MailToTicket {
    pub email: String,
}

pub struct ContactTicket {
    pub name: String,
    pub email: String,
}

pub struct WifiTicket {
    pub network: String,
    pub security: String,
    pub password: String,
}

pub struct EventTicket {
    pub name: String,
    pub start_at: String,
    pub end_at: String,
}
