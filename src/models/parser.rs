/*
 * Created by Bilal Elmoussaoui <bil.elmoussaoui@gmail.com>
 * Simple parser to allow reading QRCode content easily.
 */

use failure::Error;
use std::collections::HashMap;

pub fn parse_content(content: &str, schema: &str) -> Result<HashMap<String, String>, Error> {
    let mut output: HashMap<String, String> = HashMap::new();

    let mut modified_schema: String = String::from(schema).split_whitespace().collect();
    let mut modified_content: String = String::from(content).split_whitespace().collect();;

    let mut key_start_idx = modified_schema.find("${");
    let mut key_end_idx = modified_schema.find("}");

    while key_start_idx.is_some() && key_end_idx.is_some() {
        let (_, after_str) = modified_schema.split_at(key_start_idx.unwrap());
        let mut key = String::from(after_str.split_at(after_str.find("}").unwrap()).0);
        key.remove(0); // Remove $
        key.remove(0); // Remove {

        let mut val = String::from("");
        if let Some(char_after_key) = modified_schema.chars().nth(key_end_idx.unwrap() + 1) {
            let content = modified_content.split_at(key_start_idx.unwrap()).1;

            if let Some(char_end_idx) = content.find(char_after_key) {
                val.push_str(&content.split_at(char_end_idx).0);
                modified_content = content.split_at(char_end_idx).1.to_string();
            }
        } else {
            let content = modified_content.split_at(key_start_idx.unwrap()).1;
            val.push_str(&content.split_at(content.len()).0);
        }

        output.insert(key, val.trim().to_string());
        modified_schema = String::from(modified_schema.split_at(key_end_idx.unwrap() + 1).1);

        key_start_idx = modified_schema.find("${");
        key_end_idx = modified_schema.find("}");
    }
    Ok(output)
}

#[cfg(test)]
mod tests {
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    #[test]
    fn test_sms_content() -> Result<(), Error> {
        let content = parse_content("smsto:07777777777:TestMessage", "smsto:${tel}:${content}")?;

        assert_eq!(content.get("tel").unwrap(), "07777777777");
        assert_eq!(content.get("content").unwrap(), "TestMessage");
        Ok(())
    }

    #[test]
    fn test_tel_content() -> Result<(), Error> {
        let content = parse_content("tel:04858324823", "tel:${tel}")?;

        assert_eq!(content.get("tel").unwrap(), "04858324823");
        Ok(())
    }

    #[test]
    fn test_geo_content() -> Result<(), Error> {
        let content = parse_content("geo:52.05249047600099,-2.8125", "geo:${latitude},${longitude}")?;

        assert_eq!(content.get("latitude").unwrap(), "52.05249047600099");
        assert_eq!(content.get("longitude").unwrap(), "-2.8125");
        Ok(())
    }

    #[test]
    fn test_wifi_content() -> Result<(), Error> {
        let content = parse_content("wifi:T:WPA;S:Test;P:TestKey;;", "wifi:T:${encryption};S:${wifi};P:${key};;")?;
        assert_eq!(content.get("encryption").unwrap(), "WPA");
        assert_eq!(content.get("wifi").unwrap(), "Test");
        assert_eq!(content.get("key").unwrap(), "TestKey");
        Ok(())
    }

    #[test]
    fn test_mailto_content() -> Result<(), Error> {
        let content = parse_content("mailto:joe@bloggs.com", "mailto:${email}")?;
        assert_eq!(content.get("email").unwrap(), "joe@bloggs.com");
        Ok(())
    }
    #[test]
    fn test_contact_info_content() -> Result<(), Error> {
        let content = parse_content("MECARD:N:Joe;EMAIL:Joe@bloggs.com;;", "MECARD:N:${name};EMAIL:${email};;")?;
        assert_eq!(content.get("email").unwrap(), "Joe@bloggs.com");
        assert_eq!(content.get("name").unwrap(), "Joe");
        Ok(())
    }

    #[test]
    fn test_event_content() -> Result<(), Error> {
        let content = parse_content(
            "BEGIN:VEVENT

            SUMMARY:Christmas

            DTSTART:  20091225
            DTEND:       20091225

            END:VEVENT",
            "BEGIN:VEVENT
            SUMMARY:${event}
            DTSTART:${start_date}
            DTEND:${end_date}
            END:VEVENT",
        )?;
        //assert_eq!(content.get("event").unwrap(), "Christmas");
        assert_eq!(content.get("start_date").unwrap(), "20091225");
        assert_eq!(content.get("end_date").unwrap(), "20091225");
        Ok(())
    }
}
