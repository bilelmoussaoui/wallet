use super::object_wrapper::ObjectWrapper;
use crate::database::get_tickets;
use crate::models::Ticket;
use gio::prelude::*;
use glib::prelude::*;

pub struct TicketsModel {
    pub model: gio::ListStore,
}

impl TicketsModel {
    pub fn new() -> Self {
        let tickets_model = gio::ListStore::new(ObjectWrapper::static_type());
        let model = Self { model: tickets_model };
        model.init();
        model
    }

    fn init(&self) {
        for ticket in get_tickets().unwrap().iter() {
            self.add(&ticket);
        }
    }
    pub fn add(&self, ticket: &Ticket) {
        let object = ObjectWrapper::new(ticket);
        self.model.insert(0, &object);
    }

    pub fn delete(&self, ticket: &Ticket) {
        if let Some(idx) = self.index(ticket) {
            self.model.remove(idx);
        }
    }

    pub fn get_count(&self) -> u32 {
        self.model.get_n_items()
    }

    pub fn get_ticket(&self, index: u32) -> Ticket {
        let gobject = self.model.get_object(index).unwrap();
        let ticket_object = gobject.downcast_ref::<ObjectWrapper>().expect("ObjectWrapper is of wrong type");
        ticket_object.deserialize()
    }

    fn index(&self, ticket: &Ticket) -> Option<u32> {
        for i in 0..self.get_count() {
            let t = self.get_ticket(i);

            if &t == ticket {
                return Some(i);
            }
        }
        None
    }

    /*
    pub fn set_sorting(&self, sort_by: Option<SortBy>, sort_order: Option<SortOrder>) {
        let sort_by = match sort_by {
            Some(sort_by) => {
                self.sort_by.replace(sort_by.clone());
                sort_by
            }
            None => self.sort_by.borrow().clone(),
        };

        let sort_order = match sort_order {
            Some(sort_order) => {
                self.sort_order.replace(sort_order.clone());
                sort_order
            }
            None => self.sort_order.borrow().clone(),
        };
        let sort = (sort_by.clone(), sort_order.clone());
        self.shows_model.sort(move |a, b| Self::resources_cmp(a, b, sort.0.clone(), sort.1.clone()));
    }

    fn resources_cmp(a: &gio::Object, b: &gio::Object, sort_by: SortBy, sort_order: SortOrder) -> std::cmp::Ordering {
        let mut show_a: Show = a.downcast_ref::<ObjectWrapper>().unwrap().deserialize();
        let mut show_b: Show = b.downcast_ref::<ObjectWrapper>().unwrap().deserialize();

        if sort_order == SortOrder::Desc {
            let tmp = show_a;
            show_a = show_b;
            show_b = tmp;
        }

        match sort_by {
            SortBy::Name => show_a.get_title().cmp(&show_b.get_title()),
            SortBy::Date => show_a.get_created_at().cmp(&show_b.get_created_at()),
        }
    }
    */
}
