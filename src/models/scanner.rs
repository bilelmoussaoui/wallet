use crate::models::zbar;
use dbus::{BusType, Connection};
use failure::Error;
use std::fs;
use std::path::PathBuf;

use crate::database::Insert;
use crate::models::{NewTicket, Ticket};

lazy_static! {
    static ref CACHE_PATH: PathBuf = glib::get_user_cache_dir().unwrap().join("wallet");
}

pub struct Scanner {}

impl Scanner {
    pub fn new_from_area() -> Result<NewTicket, Error> {
        use super::screenshot::OrgGnomeShellScreenshot;
        let connection = Connection::get_private(BusType::Session)?;

        let p = connection.with_path("org.gnome.Shell.Screenshot", "/org/gnome/Shell/Screenshot", 60000);

        if let Ok((x, y, width, height)) = p.select_area() {
            info!("Capturing area ({}, {}) ({}, {})", x, y, width, height);

            let cache_path = &CACHE_PATH;
            fs::create_dir_all(&cache_path.to_str().unwrap())?;
            let tmp_file = cache_path.join("wallet_tmp_image.png");

            let tmp_path = tmp_file.to_str().unwrap().to_string();

            let (_, image_path) = p.screenshot_area(x, y, width, height, false, tmp_path.clone()).expect("Failed to screenshot an area");
            info!("Area saved {}", image_path);

            let qr_data = zbar::scan_image(&image_path)?;
            //fs::remove_file(&tmp_path)?;

            info!("Cached area {} was deleted", image_path);
            return Ok(NewTicket {
                title: "".to_string(),
                description: "".to_string(),
                data: qr_data,
            });
        }
        bail!("Failed to caputre an area");
    }
}
