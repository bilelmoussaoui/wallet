table! {
    tickets (id) {
        id -> Integer,
        title -> Text,
        description -> Text,
        data -> Text,
        created_at -> Text,
    }
}
