use crate::config;
use crate::database::{Delete, Insert};
use crate::models::{NewTicket, Scanner, Ticket};
use crate::widgets::Window;
use gio::prelude::*;
use gtk::prelude::*;
use gtk::SettingsExt;
use std::env;
use std::{cell::RefCell, rc::Rc};

pub enum Action {
    ScanQR,
    Notify(String),
    CloseNotification,
    EditTicket(Ticket),
    LoadTicket(NewTicket),
    DeleteTicket(Ticket),
}

pub struct Application {
    app: gtk::Application,
    window: Rc<Window>,
    receiver: RefCell<Option<glib::Receiver<Action>>>,
}

impl Application {
    pub fn new() -> Rc<Self> {
        let app = gtk::Application::new(Some(config::APP_ID), gio::ApplicationFlags::FLAGS_NONE).unwrap();

        let (sender, receiver) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);

        let window = Window::new(sender.clone());

        glib::set_application_name(&format!("{}Wallet", config::NAME_PREFIX));
        glib::set_prgname(Some("wallet"));

        let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/shortcuts.ui");
        let dialog: gtk::ShortcutsWindow = builder.get_object("shortcuts").unwrap();
        window.widget.set_help_overlay(Some(&dialog));

        let application = Rc::new(Self {
            app,
            window,
            receiver: RefCell::new(Some(receiver)),
        });
        application.setup_gactions();
        application.setup_signals();
        application.setup_css();
        application
    }

    fn do_action(&self, action: Action) -> glib::Continue {
        match action {
            Action::ScanQR => match Scanner::new_from_area() {
                Ok(new_ticket) => {
                    // self.do_action(Action::LoadTicket(new_ticket));
                    if let Ok(ticket) = new_ticket.insert() {
                        self.window.add_ticket(ticket);
                        self.window.refresh_view();
                    }
                }
                Err(_) => {
                    self.do_action(Action::Notify("Failed to scan the QR Code".to_string()));
                }
            },
            Action::DeleteTicket(ticket) => {
                self.window.delete_ticket(ticket.clone());
                if ticket.delete().is_err() {
                    self.do_action(Action::Notify("Failed to delete the ticket".to_string()));
                }
            }
            Action::LoadTicket(new_ticket) => {
                self.window.load_ticket(new_ticket);
            }
            Action::EditTicket(ticket) => {}
            Action::Notify(message) => self.window.notify(message),
            Action::CloseNotification => self.window.close_notification(),
        };

        glib::Continue(true)
    }

    pub fn setup_gactions(&self) {
        // Quit
        let app = self.app.clone();
        let simple_action = gio::SimpleAction::new("quit", None);
        simple_action.connect_activate(move |_, _| app.quit());
        self.app.add_action(&simple_action);
        self.app.set_accels_for_action("app.quit", &["<primary>q"]);
        self.app.set_accels_for_action("win.show-help-overlay", &["<primary>question"]);
        self.app.set_accels_for_action("window.scan-qr", &["<primary>s"]);

        // About
        let window = self.window.widget.clone();
        let simple_action = gio::SimpleAction::new("about", None);
        simple_action.connect_activate(move |_, _| {
            let builder = gtk::Builder::new_from_resource("/com/belmoussaoui/Wallet/about_dialog.ui");
            let about_dialog: gtk::AboutDialog = builder.get_object("about_dialog").unwrap();
            about_dialog.set_transient_for(Some(&window));

            about_dialog.connect_response(|dialog, _| dialog.destroy());
            about_dialog.show();
        });
        self.app.add_action(&simple_action);
    }

    pub fn setup_signals(&self) {
        let window = self.window.widget.clone();
        self.app.connect_activate(move |app| {
            window.set_application(Some(app));
            app.add_window(&window);
            window.present();
        });
    }

    pub fn setup_css(&self) {
        if let Some(gtk_settings) = gtk::Settings::get_default() {
            gtk_settings.set_property_gtk_theme_name(Some("Adwaita"));
        }

        let theme = gtk::IconTheme::get_default().unwrap();
        theme.add_resource_path("/com/belmoussaoui/Wallet/icons");

        let p = gtk::CssProvider::new();
        gtk::CssProvider::load_from_resource(&p, "/com/belmoussaoui/Wallet/style.css");
        gtk::StyleContext::add_provider_for_screen(&gdk::Screen::get_default().unwrap(), &p, 500);
    }

    pub fn run(&self, app: Rc<Self>) {
        info!("{}Wallet ({})", config::NAME_PREFIX, config::APP_ID);
        info!("Version: {} ({})", config::VERSION, config::PROFILE);
        info!("Datadir: {}", config::PKGDATADIR);

        let app = app.clone();
        let receiver = self.receiver.borrow_mut().take().unwrap();
        receiver.attach(None, move |action| app.do_action(action));

        let args: Vec<String> = env::args().collect();
        self.app.run(&args);
    }
}
