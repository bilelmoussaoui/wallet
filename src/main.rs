extern crate pretty_env_logger;
#[macro_use]
extern crate log;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate glib;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate failure;
#[macro_use]
extern crate lazy_static;

extern crate gtk;
use gettextrs::*;

mod application;
mod config;
mod database;

mod models;
//mod window_state;
mod schema;
mod widgets;

use application::Application;
use config::{GETTEXT_PACKAGE, LOCALEDIR};

fn main() {
    pretty_env_logger::init();

    gtk::init().expect("Unable to start GTK3");
    // Prepare i18n
    setlocale(LocaleCategory::LcAll, "");
    bindtextdomain(GETTEXT_PACKAGE, LOCALEDIR);
    textdomain(GETTEXT_PACKAGE);

    let res = gio::Resource::load(config::PKGDATADIR.to_owned() + "/resources.gresource").expect("Could not load resources");
    gio::resources_register(&res);
    libhandy::Column::new();

    let app = Application::new();
    app.run(app.clone());
}
