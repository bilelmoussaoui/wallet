<a href="https://flathub.org/apps/details/com.belmoussaoui.Wallet">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Wallet
Save your tickets safely

<img src="https://gitlab.gnome.org/bilelmoussaoui/raw/master/data/icons/org.bilelmoussaoui.Wallet.svg" width="128" height="128" />
<p>Save your tickets safely.</p>

## Screenshots

<div align="center">
![screenshot](data/resources/screenshots/screenshot1.png)
</div>

## Hack on Wallet
To build the development version of Wallet and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

You are expected to follow our [Code of Conduct](/code-of-conduct.md) when participating in project
spaces.

