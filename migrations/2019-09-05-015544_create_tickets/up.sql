-- Your SQL goes here
CREATE TABLE `tickets` (
    `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
    `title` TEXT NULL,
    `description` TEXT NULL,
    `data` TEXT NULL UNIQUE,
    `created_at` TIMESTAMP
    DEFAULT CURRENT_TIMESTAMP
);
